/** Copyright © 2018, Okta, Inc.
 *
 *  Licensed under the MIT license, the "License";
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     https://opensource.org/licenses/MIT
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.okta.scim.database;

import com.okta.scim.models.ProjectMembership;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Interface for the {@link ProjectMembership} database
 */
@Repository
public interface ProjectMembershipDatabase extends JpaRepository<ProjectMembership, Long> {
    /**
     * Gets a single resource from the database, matching the given ID
     * @param id The ID to search for
     * @return The instance of {@link ProjectMembership} found
     */
    List<ProjectMembership> findById(String id);

    /**
     * Searches and returns all instances of {@link ProjectMembership} that match a given project ID
     * @param projectId The project ID to search
     * @param pagable A pageable object, usually a {@link org.springframework.data.domain.PageRequest}
     * @return A {@link Page} object with the found {@link ProjectMembership} instances
     */
    @Query("SELECT pm FROM ProjectMembership pm WHERE pm.projectId = :projectId")
    Page<ProjectMembership> findByProjectId(@Param("projectId") String projectId, Pageable pagable);

    @Query("SELECT pm FROM ProjectMembership pm WHERE pm.userId = :userId")
    Page<ProjectMembership> findByUserId(@Param("userId") String userId, Pageable pageable);

    /**
     * Searches and returns all instances of {@link ProjectMembership} that match a given project ID and userId
     * @param projectId The project ID to search
     * @param userId The userId to search
     * @param pagable A pageable object, usually a {@link org.springframework.data.domain.PageRequest}
     * @return A {@link Page} object with the found {@link ProjectMembership} instances
     */
    @Query("SELECT pm FROM ProjectMembership pm WHERE pm.projectId = :projectId AND pm.userId = :userId")
    Page<ProjectMembership> findByProjectIdAndUserId(@Param("projectId") String projectId, @Param("userId") String userId, Pageable pagable);
}
