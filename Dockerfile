FROM openjdk:8-jdk-alpine

ENV JAVA_OPTS=""
ENV DOC_ROOT /scim_server
ENV VER=1.0
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV FILENAME scim-server

RUN mkdir -p /root/.ssh
RUN chmod 700 /root/.ssh

RUN mkdir -p /scim_server   \
             /Logs

ADD ./target/${FILENAME}-1.0.jar /scim_server/${FILENAME}-1.0.jar

USER root
ENV TZ 'Asia/Seoul'
RUN echo $TZ > /etc/timezone
RUN echo ${PROFILE}
#EXPOSE
ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -Dspring.profiles.active=${PROFILE} -Djava.security.egd=file:/dev/./urandom -jar /scim_server/${FILENAME}-1.0.jar --spring.config.location=file:///scim_server/application.yaml"]


